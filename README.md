
Esse é um projeto pessoal que visa verificar a seguinte hipótese:

> Sou capaz de criar uma página no gitlab usando apenas o terminal em 14 dias me dedicando em média meia hora por dia pra isso
<!-- <details> -->
<!-- <summary> Resumo dos dias </summary> -->
<!-- toc -->
<!-- toc stop -->
<!-- </details> -->

## Quem sou eu?

Meu nome é **Teles**, sou desenvolvedor front-end faz uns 8 anos. Sou usuário de Linux faz 10 anos.
Apesar disso conheço muito pouco o VIM mas ouvi dizer que ele é poderoso.

Será que em 14 dias eu aprendo a usa-lo? Sei lá.
Vou deixar aqui o registro da experiencia.

## Dia 1

Instalei o vim no ubuntu 16.

``` 
sudo apt-get install vim
``` 

* Criei o arquivo README.md assim `vim README.md`;
* Descobri que apertar I entra em modo de INSERÇÃO;
* Criei a primeira versão desse arquivo, que não é essa versão!
* Não sabia como fechar o editor salvando o arquivo e usei *:quit!*, resultado não salvou nada e tive que reescrever!!
* Ainda não sei como fechar esse arquivo :O


### Dia 1. Salvando o arquivo

[Ainda bem que aprendí aqui que :wq fecha e salva o arquivo](https://stackoverflow.com/a/11828573);

O ctrl+shift+v também funciona. É parecido com o nano.

### Dia 1. Coisas que fiz depois de conseguir salvar esse arquivo

Lembrando. Salva-se arquivo usando **esc** seguido de **wq**.

* Editei o arquivo .gitlab-ci.yml e consegui gerar a página [desde repositório](http://joseteles.gitlab.io/fepuvim).
* Editei esse README.md novamente

### Dia 1. Coisas que vou investigar no vim:

* Como usar ctrl+c ctrl+v no vim sem mouse;
* Como corrigir a largura do tab; aqui está em 8 espaços;
* Como buscar dentro de um arquivo com vim;

## Dia 2

* Pensando em dar alguma finalidade para esse repositório;
* Acredito que possivelmente tentarei fazer AMP stories aqui;

### Dia 2. Tive problemas

* Erros de espaçamento no arquivo de CI do gitlab tornaram o arquivo inválido;
* Meu VIM está configurado para tabs de 8 espaços, como arrumar isso?


### Dia 2. VIM para noobs

* Comecei a dar uma lida no [Vim para Noobs](https://woliveiras.com.br/vimparanoobs/chapters/inicio.html) da lenda do front-end William Oliveira.
* Quase deletei tudo denovo pois fui usar o comando `v` para entrar em Visual Mode. Ai deletei coisas sem querer, por sorte lembrei do `:quit!` e sai sem salvar o arquivo.

### Dia 2. Aprendi umas coisinhas

* Consegui usar o modo visual, basta apertar esc depois v;
* Entra-se então em modo de seleção de textos, com isso copiei o conteúdo aqui do dia 2 e o coloquei para cima do dia 1.


### Dia 2. Recortando texto
* Entrar em modo visual com o v;
* Selecionar o texto que deseja recortar;
* Recorta-lo com o d;
* Ir até onde deseja colar o texto;
* Apertar p no modo visual para colar o texto!

## Dia 3

* Descobri que o VIM aceita alguns comandos como  o `:undo`;
* Descobri que `:undo2` desfaz as duas ultimas ações;
* Criei um novo arquivo [amp-list.html](amp-experiments/amp-list.html);
* Criei um json com VIM também, a intenção era aprender a usar um componente AMP chamado amp-list

### Dia 3. Usando VIM no Mac

* Testando o VIM no MAC descobri que os arquivos não estão vindo com highlight
* Ainda não descobri como mudar o padrão de tamanho dos tabs

### Dia 3. O tal do ~/.vimrc

* Achei um arquivo de configuracao para o VIM, ele é o tal ~/.vimrc
* Peguei um .vimrc pronto da net e estou usando. Agora as cores aparecem quando abro um .md ou .html
* Para abrir um outro arquivo basta fazer `:e nome-do-arquivo`;
* Para fazer uma busca basta fazer `\ busca`

### Dia 3. Quero aprender isso:

* Quero aprender a fechar tags automaticamente no vim;
* Quero aprender a formatar um arquivo automaticamente no vim;

Seria bem útil!

## Dia 4

Crie alguns experimentos com AMP usando vim:

* [AMP List](amp-experiments/amp-list.html)
* [AMP List com dados de decoração](amp-experiments/amp-list-decoracao.html)
* [AMP Stories básico](amp-experiments/amp-stories-hello-world.html)
* [AMP Stories com AMP List - Esse não funcionou como esperado](amp-experiments/amp-list-stories-decoracao.html)

Penso em criar alguns experimentos parecidos com Nuxt também.
Intenção é aprender o vim de forma colateral, ou seja, faço meu trabalho habitual usando o vim ao invés de focar diretamente só nele.
Será que funciona?

### Dia 4. Tem muita coisa para aprender!

Olha só quanta coisa só sobre como [mover o cursor no vim](https://woliveiras.com.br/vimparanoobs/chapters/movendo-o-cursor.html).

### Dia 4. Comandos mais fáceis de lembrar

* `$` é fácil de lembrar por causa das regex. significa ir ao fim da linha em ambos os casos
* `0` para começo da linha também é bastante intuitivo
* `b` para voltar uma palavra
* `w` para próxima palavra

## Dia 5

* Adicionei um diretório para fazer testes com VueJS/Nuxt.
* Estou me adaptando ao `w` e `b` para andar no texto mais rapidamente;
* Hipótese que verificarei com o tempo:  usar o vim talvez ajude em concentração?

### Dia 5 - Sem sucesso em instalar plugin para dar highlight no VueJS

* Instalei o [Vundle](https://github.com/VundleVim/Vundle.vim) na intenção de poder instalar um plugin para hightlight de VueJS 
* Adicionei o [plugin para VueJS](https://github.com/storyn26383/vim-vue)
* Rodei o comando que instala plugin `vim +PluginInstall +qall`
* Nada de diferente quando abri o arquivo pages/index.vue, nada de sintaxe colorida :
* [Minha versão atual do .vimrc](.vimrc-versao-1)

### Dia 5 - Esse cara manja de vim

* Lendo [esse artigo]( https://medium.com/@schtoeffel/you-don-t-need-more-than-one-cursor-in-vim-2c44117d51db)
pensei como o vim pode ser produtivo

### Dia 5 - Abrindo o emacs de brincadeira

* Sem sucesso em instalar o plugin de VueJS (veja acima) rodei `emacs pages/index.vue`. Resultado: tive que fechar o terminal para sair do arquivo hehehe

### Dia 5 - Consegui instalar o danado do plugin de VueJS

* Aparentemente o problema era o fato de faltar um trecho da configuração do Vundle depois da instalação de plugins, [eis o trecho que faltava](https://gitlab.com/joseteles/projeto-14-dias-vim/blob/master/.vimrc-versao-2#L19-21).
* Rodei `vim pages/index.vue` e agora estava com highlight de script e template :D
* [Essa é a versão atual do meu ~/.vimrc com o VueJS funcionando](.vimrc-versao-2)

## Dia 6

Fiz outros experimentos tentando unir <amp-list> e <amp-stories>, sem sucesso. :(
Tive algumas dúvidas básicas sobre vim depois de conversar com o amigo João Paulo.
Voltei ao Ubuntu e configurei o vim nele

### Dia 6 - Experimentos AMP falharam

Criei alguns experimentos com a intenção de usar o <amp-list> componente AMP que lê json e o manipula usando mustache e <amp-stories>. A ideia é pegar um endpoint de fotos e criar um <amp-stories> dele.

Vou abrir uma issue no [repositório github do projeto amp](https://github.com/ampproject/amphtml). Se não der para resolver com AMP vou apelar para o NUXT.

### Dia 6 - Será que o vim faz essas coisas?

* Consigo configurar o vim para navegar para a especificação de uma função ao seleciona-la ?
* Consigo configurar o vim para trocar o nome de uma função nos lugares em que ela é usada depois de alterar sua especificação?

### Dia 6 - Voltando ao ubuntu e configurando o ambiente para usar vim

Voltei ao Ubuntu!
Crie um [repositório para meus arquivos de configuração](https://github.com/teles/dotfiles/), até a data dessa publicação ele contém apenas uma [cópia do meu arquivo de configuração do vim](.vimrc-versao-2).

Para configurar o vim e este projeto rodei os comandos a seguir:

```
# instalando o Vundle
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim

# Configurando o vim
wget https://github.com/teles/dotfiles/archive/master.zip
unzip master.zip
cp dotfiles-master/.vimrc ~/.vimrc
rm -Rf master.zip dotfiles-master
vim +PluginInstall +qall

```

E pronto, vim configurado.

## Dia 7

Estou escrevendo com um dia de atraso aqui. Porém no dia 7 eu continuei no projeto vim.

### Dia 7. Video do Linux Tips

Assisti esses dois videos sensacionais sobre vim, eles estão em português:
<iframe width="560" height="315" src="https://www.youtube.com/embed/XXRZ0acXHU0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
<iframe width="560" height="315" src="https://www.youtube.com/embed/_qShvkx8jK0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Descobri entre outras coisas que é fácil setar o tamanho dos tabs no vim.

### Dia 7. Ideias para esse projeto

* Adicionar OneSignal aqui para notificar sobre atualizações
* Criar uma lista dos comandos que peguei nos videos do LinuxTips
* Transformar esse index em um  `<amp-story>`
* Adicionar os prints e aúdio que tenho feito sobre o projeto aqui nessa página

### Dia 7. "Vim is the perfect IDE"

[Vim is the perfect IDE](https://dev.to/allanmacgregor/vim-is-the-perfect-ide-e80). Gostei desse artigo

## Dia 8 

Conheci o [Vim Bootstrap](https://vim-bootstrap.com/) e tentei tornar esse AMP aqui válido sem sucesso.

### Dia 8. AMPify

* Tentei usar o [ampify](https://www.npmjs.com/package/ampify) para tornar essa página um AMP válido. Sem sucesso. 

### Dia 8. Medium CLI

* Criei [um post de olá mundo](https://medium.com/@jotateles/ol%C3%A1-mundo-b0825e7e9300) usando o vim e postei no medium

## Dia 9

Lembrando do dia 3 fui ver como fechar tags html. O processo é simples, tive que adicionar uma linha no `.vimrc`.

```
echo ":iabbrev </ </<C-X><C-O>" >> ~/.vimrc
```

* [Artigo sobre fechamento de tag no vim](http://vim.wikia.com/wiki/Auto_closing_an_HTML_tag)

Mas não consegui fazer com que o Espaço fechasse tag, apenas consigo usar o fechamento automático usando Ctrl+x e Ctrl+o.

### Dia 9. EditorConfig

* Fui pesquisar se existia um jeito de usar o [EditorConfig](https://editorconfig.org/) com o Vim. 
* Existe sim, esse plugin de [EditorConfig para Vim](https://github.com/editorconfig/editorconfig-vim).
* Instala-lo é fácil. Tive que entrar no meu ~/.vimrc e adicionar uma linha na lista de plugins. Essa linha `Plugin 'editorconfig/editorconfig-vim'`.
* [Versão mais atualizada do meu .vimrc](.vimrc-versao-3).

Graças ao EditorConfig agora meus tabs estão do tamanho certo dentro do javascript :)
Quando estou em uma função e aperto o enter o cursor não vai pro começo da próxima linha e sim para posição relacionada a indentação.

## Dia 10

* Percebi que estou usando bem o `%s/busca/troca/`, é fácil.
* Resolvi que vou dar uma melhorada no layout dessa página e fazer a versão AMP separada;
* Também decidi trocar o nome desse respositório para "Vim do mundo".

### Dia 10. Melhorando o layout

Usando apenas as classes do bulma.io comecei a colocar um cabeçalho aqui na página.

### Dia 10. Editando um arquivo logado no servidor

* Finalmente fui capaz de editar um arquivo logado direto no servidor via ssh.
* Fiz uma pequena alteração de css no [blog da minha companheira](http://natalia.blog.br) via terminal usando vim, coisa que tempos atrás eu teria dificuldade grandes em fazer.

## Dia 11

Tirei o dia para melhorar o design dessa página pois estava descontente com a versão anterior.

### Dia 11. Find and replace

Nada de novidades mas conseguí usar bem o find and replace dentro do README.md depois que eu movi vários arquivos que estavam linkados no README.md para outro diretório.

### Dia 11. Ir para o começo da linha

Percebi que eu perco bastante tempo voltando ao começo da linha então descobri que tem um jeito mais fácil de resolver isso. Basta ir pro modo normal e apertar 0 para ir ao começo da linha.

### Dia 11. Direto do vídeo do LinuxTips. Inserção

* `a` insere um caractere a frente; é como o `i` mas começa a inserção pra frente;
* `I` começa a inserir no começo da linha
* `A` começa a inserir no final da linha
* `o` começa a inserir no começo da linha seguinte
* `O` começa a inserir na linha de cima
* `S` apaga a linha atual e começa a inserir

### Dia 11. Direto do vídeo do LinuxTips. Dicas

* `:wq` pode ser simplificado por `x`
* `:wq` também pode ser substituído por shift + ZZ  
* `yy` copia a linha inteira
* `p` cola o conteudo copiado ou recortado
* `dd` recorta a linha 
* `3dd` apaga 3 linhas
* `yw` copia uma palavra
* `3yw` copia 3 palavras
* `:w nome-do-arquivo` é um "Salvar como" que salva conteúdo atual em nome-do-arquivo

## Dia 12

Usei o vim no trabalho para editar arquivos de redirects do nginx. Ele ajudou.
Porém não procurei novidades sobre o vim exceto o cheat sheet que encontrei online.

### Dia 12. Cheatsheet

Descobri esse cheatsheet aqui no site [http://www.viemu.com/](http://www.viemu.com/).

![Imagem do vim CheatSheet](images/vi-vim-cheat-sheet.gif)

Consegui baixa-lo e adiciona-lo aqui sem sair do vim. Como?
Dentro do vim rodei o comando `! wget http://www.viemu.com/vi-vim-cheat-sheet.gif`. 
O vim me mandou para o bash e depois me pediu para apertar um enter e continuar a edição dessa página. Simples.

### Dia 12. Trabalhando com comandos externos

Como falei no relato sobre cheatsheet (acima) aprendi a [trabalhar com comandos no vim nesse site](https://www.linux.com/learn/vim-tips-working-external-commands).

### Dia 12. Cheatsheet em português 

Aqui uma [lista dos comandos vim e o que significam em português do Brasil](https://vim.rtorr.com/lang/pt_br/).

## Dia 13

Aproveitei o dia 13 para organizar esse readme. Se antes ele começava do dia mais recente e seguia até o dia mais antigo, reformatei para que fosse o contrário.

### Dia 13. Navegando, recortando texto e colando

Para reorganizar o README tive de manipular praticamente o texto todo. Para isso eu fiz assim:

```
vim README.md
``` 

* Uma vez no arquivo eu usei `GA` no modo normal para ir ao fim do texto.
* Lá apertei `V` para entrar em modo VISUAL/LINHA.
* No modo VISUAL eu selecionei as linhas que desejava
* Selecionadas as linhas apertava `d` para recorta-las
* Apertava `gg` para ir ao começo do arquivo
* Apertava `p` para colar as linhas


* Também usei o `{` e o `}` no modo normal para navegar respectivamente para parágrafo anterior e parágrafo seguinte. O que me ajudou bastante.

### Dia 13. Lembranças do Shell

Esse papo de vim acabou me lembrando de um livro empoeirado do ano de 2008: Programação Shell Linux do mestre Julio Cesar Neves.
Tenho a impressão que programar naquele tempo tinha uma pegada um pouco diferente, era fascinante escrever pequenos programas para manipular texto.
O primeiro repositório que tive no GitHub veio dessa época, era uma linha de comando escrita em awk para adiantar e atrasar legendas de filmes! 

Hoje tenho outros prazeres diferentes na programação e nem lembro awk, mas era bem legal fazer aquilo hehe
Era esse [livro](https://www.amazon.com.br/Programa%C3%A7%C3%A3o-Shell-Linux-Julio-Cezar/dp/8574526886).

Também acabei me lembrando do [Aurélio](http://aurelio.net) com o qual aprendi várias coisas no meu começo em programação. Achei um [texto dele de 10 anos atrás sobre seus 30 anos](http://aurelio.net/blog/2007/10/07/fiz-trinta/) idade que farei em breve. :O

## Dia 14.

Demorei 3 dias para efetivamente escrever aqui no dia 14.
Por que não escrevi antes?

* Um pouco por ser difícil saber como terminar o projeto, pois não estou 100% satisfeito com o critério de pronto do projeto. Afinal fiz uma página mas foi bastante simples. Queria mesmo ser aqueles hackers de filme da sessão da tarde editando vários arquivos pelo terminal.

### Dia 14. Coisas que aprendi

Entre as coisas que eu sei fazer agora e não sabia no começo desse projeto:
* Sei abrir, fechar e salvar um arquivo com vim!
* Sei que existe um .vimrc para configurações do vim;
* Sei que existem vários modos no vim: normal, bloco e inserção de texto;
* Sei usar o comando `%s/busca/troca/` para usar o querido sed dentro do vim;
* Sei que existem plugin para vim;
* Sei que existem muitos plugins para vim, até mesmo para edição de single file components do VueJS;
* Sei que existem muitas teclas de atalho no vim;
* Sei me locomover razoavelmente no modo normal, usando atalhos como `0` para ir ao começo da linha e `GA` para ir ao final de um arquivo;
* Sei copiar, recortar e colar trechos e blocos de texto;
* Sei configurar um atalho de teclado especial para fechar automaticamente tags html;
* Sou capaz de editar um arquivo markdown, bash, html e javascript pelo vim;
* Sei procurar um trecho de texto usando o comando `/`
* Sei responder perguntas básicas de amigos sobre vim (tem que ser básica :P)
* Sei usar o comando `:split nome-de-outro-arquivo` para abrir dois arquivos simultaneamente no vim;
* Sei navegar entre dois arquivos abertos no vim usando CTRL + W;
* Sei logar em um ssh e editar um arquivo apenas pelo vim

### Dia 14. Coisas que eu não aprendi ainda e talvez eu aprenda

* Não sei como faço abrir um projeto dentro do vim ao invés de apenas um arquivo avulso;
* Não sei se o vim fornece através de plugin a comodidade de autocomplete;
* Não sei como faço para usar outros comandos de manipulação de texto com o vim, por exemplo o cut;
* Não sei navegar rapidamente entre trechos de código ou texto;
* Não sei fazer fold/unfold de trechos de código;
* Não sei como fazer salvamente automático frequente de arquivos;
* Não sei como posso usar o vim para editar um arquivo .docx, por exemplo (embora ache que deve ser fácil já que é apenas um xml);
* Não sei como usar eslint dentro do vim para marcar trechos de código inadequados;
* Não sei como fazer commits sem sair de dentro do vim;
* Não sei qual as grandes diferenças entre vim e emacs;
* Não sei fazer find and replace em vários arquivos;
* Não sei fazer o trim automático em um arquivo;
* Não sei se tem como adicionar features para edição de arquivos csv no vim.

### Dia 14. Deu certo usar o vim?

Provei minha premissa inicial e construí essa página usando apenas o vim.

O que eu achei disso?
Bem divertido e potencialmente útil!

Depois de usar Notepad++ muito tempo atrás, Sublime Text e atualmente usar o WebStorm penso que o vim é o editor dentre estes com maior estabilidade. Ele não faz tanto quanto os outros mas tudo que faz é bem feito.

A experiência no vim é mais imersiva do que a experiência em outro editor como o WebStorm. O vim também abre mais rápido.
A impressão é que saber o vim é como saber fazer seu próprio almoço. As vezes você pode almoçar em restaurantes e pagar por isso (com dinheiro e recursos de máquina) mas é e sempre será útil saber se virar no dia a dia com os recursos mais básicos e focado apenas no que precisa ser feito.

Tenho uma impressão, notoriamente pouco embasada de que o vim irá durar mais que outros editores mais modernos. 
E a capacidade de editar arquivos em um terminal numa sessão de ssh é frequentemente útil.

### Dia 14. Eu recomendaria a você que também aprendesse um pouco sobre vim?

Com toda certeza! Só não esqueça de me contar como foi.

Abraços e bom vim do mundo para você.
