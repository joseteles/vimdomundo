#!/usr/bin/env bash

mkdir .public
node node_modules/readme-toc/bin/toc
node readme-to-index.js > body.html
sed -i -e "s/<\!-- \(<[/]*details>\) -->/\1/g" body.html
sed -i -e "s/<\!-- \(<summary>.*<\/summary>\) -->/\1/g" body.html
cat header.html body.html footer.html > index.html
cp -R node_modules/bulma/css/bulma.min.css index.html images/ amp-experiments/ *.json .public
mv .public public

