var fs = require('fs');
var MarkdownIt = require("markdown-it");
var mdnh = require("markdown-it-named-headers");
var md = new MarkdownIt({html: true});
md.use(mdnh, {});


fs.readFile("README.md", 'utf8', function (err,data) {
    if (err) {
        return console.log(err);
    }
    console.log(md.render(data));
});
