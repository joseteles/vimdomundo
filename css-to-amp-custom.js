import path from 'path';
import express from 'express';
import inlineCSSAMP from 'express-inline-css-amp';

const app = express();
app.use(inlineCSSAMP({
    CSSFilePath: path.join(__dirname,'bulma.min.css')
}));

app.get('/', (req, res) => {
    res.render('index', {});
});

